import React from 'react';
import PropTypes from 'prop-types';

export default function Navbar(props) {
  return (
    <div>
        <nav>
            <a href='/'>{props.title}</a>
        </nav>
    </div>
  )
}
// functional base component

Navbar.propTypes = {title:PropTypes.string};

Navbar.defaultProps= { title: 'Contact'};


// file for props